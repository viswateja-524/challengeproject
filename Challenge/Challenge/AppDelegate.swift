//
//  AppDelegate.swift
//  Challenge
//
//  Created by DV Reddy on 23/12/16.
//  Copyright © 2016 DV Reddy. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var artistName: String?
    var albumName: String?
    var TrackName: String?
    var imageViewAlbumUrl: String?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        return true
    }

}

