//
//  Constants.swift
//  Challenge
//
//  Created by DV Reddy on 23/12/16.
//  Copyright © 2016 DV Reddy. All rights reserved.
//

import Foundation
import UIKit

let BASE_URL = "https://itunes.apple.com/search?term="
let appdelgate = UIApplication.shared.delegate as! AppDelegate
let customTableViewCellIdentifier = "CustomTableViewCell"
let detailScreen = "detailScreen"
let json = "json"
let connectivityMessage = "Please check internet connectivity"
let somethingWentWrong = "Something went wrong!"
let couldNotLocateFile = "Couldn't locate the file"

let space = " "
let plus  = "+"

struct music {
    static let results = "results"
    static let keyTrackName     = "trackName"
    static let keyartistName    = "artistName"
    static let keyAlbumName     = "trackCensoredName"
    static let keyAlbumImage    = "artworkUrl60"
}

struct detailMusic {
    static let keyArtist = "artist"
    static let keylyrics = "lyrics"
    static let keySong = "song"
}
