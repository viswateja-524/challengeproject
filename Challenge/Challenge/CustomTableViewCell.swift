//
//  CustomTableViewCell.swift
//  Challenge
//
//  Created by DV Reddy on 24/12/16.
//  Copyright © 2016 DV Reddy. All rights reserved.
//

import Foundation
import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTrackName: UILabel!
    @IBOutlet weak var labelArtistName: UILabel!
    @IBOutlet weak var labelAlbumName: UILabel!
    @IBOutlet weak var imageAlbum: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
