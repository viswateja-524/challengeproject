//
//  DetailViewController.swift
//  Challenge
//
//  Created by DV Reddy on 24/12/16.
//  Copyright © 2016 DV Reddy. All rights reserved.
//

import Foundation
import UIKit
import CoreFramework

class DetailViewController: UIViewController{

    @IBOutlet weak var labelAtrist: UILabel!
    @IBOutlet weak var labelLyrics: UILabel!
    @IBOutlet weak var labelSong: UILabel!
    
    @IBOutlet weak var labelAlbumName: UILabel!
    @IBOutlet weak var labelArtistName: UILabel!
    @IBOutlet weak var labelTrackName: UILabel!
    @IBOutlet weak var imageAlbum: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showDetailScreen()
    }
    
    func showDetailScreen() {
        labelAlbumName.text = appdelgate.albumName
        labelArtistName.text = appdelgate.artistName
        labelTrackName.text = appdelgate.TrackName
        if let url = appdelgate.imageViewAlbumUrl {
            loadImageFromUrl(url: url  as String, view: imageAlbum, completion: { isError in
                if isError {
                    //show alert
                }
            })
        }
        
        if let url = Bundle.main.url(forResource: detailScreen, withExtension: json) {
            
            if let data = NSData(contentsOf: url) {
                do {
                    let object = try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments)
                    if let json = object as? [String: AnyObject] {
                        
                        labelAtrist.text = json["\(detailMusic.keyArtist)"] as! String?
                        labelLyrics.text = json["\(detailMusic.keylyrics)"] as! String?
                        labelSong.text = json["\(detailMusic.keySong)"] as! String?
                    }
                } catch {
                    self.showAlertWith(message:couldNotLocateFile)
                }
            }
        }
        
    }
}
