//
//  ViewController.swift
//  Challenge
//
//  Created by DV Reddy on 23/12/16.
//  Copyright © 2016 DV Reddy. All rights reserved.
//

import UIKit
import CoreFramework

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {

    /*Outlets*/
    @IBOutlet var tableView: UITableView!
    @IBOutlet var searchBar: UISearchBar?
    @IBOutlet var actInd: UIActivityIndicatorView?
    
    var arrayAlbumName = [String]()
    var arrayTrackName = [String]()
    var arrayArtistName = [String]()
    var arrayOfImages = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.isMultipleTouchEnabled = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = true
        searchBar?.delegate = self
        actInd?.hidesWhenStopped = true
        
    }
    
    // Search Bar - Search button clicked
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let searchText = searchBar.text {
            actInd?.startAnimating()
            let appendedText = BASE_URL + searchText.replacingOccurrences(of: space, with: plus)
            genericWebserviceHelper(requestUrlString: appendedText, completion: { (dict, isError) in
                self.actInd?.stopAnimating()
                self.actInd?.isHidden = true
                if isError {
                    self.showAlertWith(message: connectivityMessage)
                }else{
                    if let dict = dict {
                        self.sortFromSearchResult(json: dict)
                        self.actInd?.stopAnimating()
                        self.actInd?.isHidden = true
                    }else{
                        self.showAlertWith(message:somethingWentWrong )
                    }
                }
            })
        }
    }
    
    // Search Bar - text tracking
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
    }
    
    // Sorting all required objects to show on table view
    func sortFromSearchResult(json: [String: Any]) {
        extractResults(resultsJsonObject: json["\(music.results)"] as? [[String : Any]], sortWithKeyName: music.keyAlbumName, completion: { albumArray in
            self.arrayAlbumName = albumArray
        })
        
        extractResults(resultsJsonObject: json["\(music.results)"] as? [[String : Any]], sortWithKeyName: music.keyTrackName, completion: { arrayTrack in
            self.arrayTrackName = arrayTrack
        })
        
        extractResults(resultsJsonObject: json["\(music.results)"] as? [[String : Any]], sortWithKeyName: music.keyartistName, completion: { arrayArtist in
            self.arrayArtistName = arrayArtist
        })
        
        extractResults(resultsJsonObject: json["\(music.results)"] as? [[String : Any]], sortWithKeyName: music.keyAlbumImage, completion: { arrayImages in
            self.arrayOfImages = arrayImages
        })
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    //Tableview Data Source - Number Of Sections
    
    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    //Tableview Data Source - Number Of Rows In Section - Required
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (arrayAlbumName.count == 0) ? 0: arrayAlbumName.count
    }
    
    //Tableview Data Source - cell For Row At indexPath - Required
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: customTableViewCellIdentifier, for: indexPath as IndexPath) as! CustomTableViewCell
        cell.selectionStyle = .none
        let row = indexPath.row
        cell.labelAlbumName?.text = arrayAlbumName[row]
        cell.labelTrackName?.text = arrayTrackName[row]
        cell.labelArtistName?.text = arrayArtistName[row]
        
        DispatchQueue.main.async {
            loadImageFromUrl(url: self.arrayOfImages[row], view: cell.imageAlbum, completion: { isError in
                if isError {
                    self.showAlertWith(message:connectivityMessage)
                }
            })
        }
        
        return cell
    }
    
    //Tableview Delegate - Did Select Row At indexPath
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        appdelgate.albumName = arrayAlbumName[row]
        appdelgate.artistName = arrayArtistName[row]
        appdelgate.TrackName = arrayTrackName[row]
        appdelgate.imageViewAlbumUrl = arrayOfImages[row]
    }
    
}
