//
//  CoreFramework.h
//  CoreFramework
//
//  Created by DV Reddy on 27/12/16.
//  Copyright © 2016 DV Reddy. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CoreFramework.
FOUNDATION_EXPORT double CoreFrameworkVersionNumber;

//! Project version string for CoreFramework.
FOUNDATION_EXPORT const unsigned char CoreFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CoreFramework/PublicHeader.h>


