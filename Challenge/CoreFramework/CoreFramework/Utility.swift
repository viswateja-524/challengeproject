//
//  Utility.swift
//  Challenge
//
//  Created by DV Reddy on 25/12/16.
//  Copyright © 2016 DV Reddy. All rights reserved.
//

import Foundation
import UIKit

extension  UIViewController {
    
    /* Sort Json object's and add all objects which are with same key into one array
        response Json Object - Result Json Object
        sortWithKeyName - find key and add into array
         completion: completion to get the array
     */
    public func extractResults(resultsJsonObject: [[String:Any]]? , sortWithKeyName: String, completion: @escaping ([String]) -> (Void)) {
        var addToNewArray = [String]()
        if let results: [[String:Any]] = resultsJsonObject as [[String:Any]]?{
            for sort in results {
                print("\(sort["\(sortWithKeyName)"])")
                if let item = sort["\(sortWithKeyName)"] as? String {
                    addToNewArray.append(item)
                }
            }
        }
        print("Total \(sortWithKeyName): \(addToNewArray.count)")
        completion(addToNewArray)
    }
    
    //Alert
    
    public func showAlertWith(message: String) {
        let alertController = UIAlertController.init(title: "Alert", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }))
        present(alertController, animated: true, completion: nil)
    }
}
