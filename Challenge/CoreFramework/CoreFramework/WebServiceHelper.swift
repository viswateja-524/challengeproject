//
//  WebServiceHelper.swift
//  Challenge
//
//  Created by DV Reddy on 25/12/16.
//  Copyright © 2016 DV Reddy. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration

/**
 Check internet connectivity
 returns true if available else false
 */
public func isNetworkAvailable() -> Bool {
    var socketZeroAddress = sockaddr_in()
    socketZeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: socketZeroAddress))
    socketZeroAddress.sin_family = sa_family_t(AF_INET)
    
    let routeReachability = withUnsafePointer(to: &socketZeroAddress) {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    }
    
    var reachabilityFlags = SCNetworkReachabilityFlags()
    if !SCNetworkReachabilityGetFlags(routeReachability!, &reachabilityFlags) {
        return false
    }
    
    let isReachable = (reachabilityFlags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
    let needsConnection = (reachabilityFlags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
    
    return (isReachable && !needsConnection)
}


/*for image's to load on imageview
 url - send image url.
 view - for which image view need to display.
 */

public func loadImageFromUrl(url: String, view: UIImageView, completion: @escaping (Bool) -> (Void)) {
    
    // Create Url from string
    if let url = NSURL(string: url) {
        
        //check if the internet is available before fetching the image data
        if isNetworkAvailable() {
            let task = URLSession.shared.dataTask(with: url as URL) { (responseData, responseUrl, error) -> Void in
                // if responseData is not null...
                if let data = responseData {
                    // execute on main thread
                    DispatchQueue.main.sync(execute: { () -> Void in
                        view.image = UIImage(data: data)
                    })
                } else if let _ = error {
                    completion(true)//error while fetching image data
                }
            }
            task.resume()//run the task
        }else{
            completion(true)//No internet available
        }
    }
}

/*for api's
 RequestUrlString - to pass complete request url
 return's - Response Data
 */
public func genericWebserviceHelper(requestUrlString: String, completion: @escaping ([String: Any]?, Bool) -> (Void)) {
    let config = URLSessionConfiguration.default // Session Configuration
    let session = URLSession(configuration: config) // Load configuration into Session
    let url = URL(string: requestUrlString)
    
    //Check for internet
    if isNetworkAvailable() {
        let task = session.dataTask(with: url!, completionHandler: {(data, response, error) in
            guard let data = data, error == nil else { return }
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)  as? [String : Any] {
                    completion(json, false)
                }
            } catch _ as NSError {
                completion(nil, true)//error while reaching endpoint
            }
        })
        task.resume()//run the task
    }else{
        completion(nil, true)//No internet available
    }
    
}

